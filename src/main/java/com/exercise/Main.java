package com.exercise;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException{
        sendGetReq();
    }

    public static JSONArray sendGetReq() throws IOException {
        String line = "";
        String paragraph = "";
        System.out.println("Enter the text, (Press Enter twice to exit): ");
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(isr);
        do {
            line = bufferedReader.readLine();
            paragraph = paragraph + line + " ";
        } while (!line.equals(""));
        isr.close();
        bufferedReader.close();
        String str[] = paragraph.split(" ");
        List<String> al = Arrays.asList(str);
        System.out.println(al);
        return getRequest(al);
    }

    public static boolean isUrlValid(String url) {
        try {
            new URL(url).toURI();
            return true;
        }
        catch (Exception e) {
            System.err.println("Bad address: "+ url);
            return false;
        }
    }

    public static JSONObject getFullResponse(HttpURLConnection con) throws IOException {
        StringBuilder fullResponseBuilder = new StringBuilder();
        JSONObject data = new JSONObject();
        try {
            con.setConnectTimeout(10000);
            con.setReadTimeout(10000);
            data.put("Url", con.getURL().toString().replaceAll("\\+", ""));
            data.put("Status_code", con.getResponseCode());
            data.put("Content_length", con.getContentLength());
            data.put("Date", con.getHeaderField("Date"));
        }
        catch (UnknownHostException exception){
            data.put("URL", con.getURL().toString().replaceAll("\\+", ""));
            data.put("Error", "Invalid URL");
        }
        System.out.println(con.getURL().toString().replaceAll("\\+", ""));
        System.out.println(data);
        fullResponseBuilder
                .append(data)
                .append("\n");
        return data;
    }

    public static JSONArray getRequest(List<String> al) throws IOException {
        JSONArray finalData = new JSONArray();
        for (String s: al){
            // Remove any white space in url
            String url = s.replaceAll("\\s+","");
            if (isUrlValid(url)){
                URL urlPath = new URL(url);
                HttpURLConnection con = (HttpURLConnection) urlPath.openConnection();
                getFullResponse(con);
                finalData.add(getFullResponse(con));
            }else {
                JSONObject badURL = new JSONObject();
                System.err.println("Invalid URL");
                badURL.put("Url", url);
                badURL.put("Error", "Invalid URL");
                finalData.add(badURL);
            }
        }
        try (FileWriter file = new FileWriter("results.json")) {
            file.write(finalData.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(finalData);
        return finalData;
    }
}
