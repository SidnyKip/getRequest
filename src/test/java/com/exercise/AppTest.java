package com.exercise;

import junit.framework.TestCase;
import org.hamcrest.core.Is;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class AppTest  extends TestCase {

    public String getDate(){
        String pattern = "E, d MMM yyyy HH:mm:ss 'GMT'";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en"));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String date = simpleDateFormat.format(new Date());
        return date;
    }

    public void testOneUrl() throws IOException {
        Main myUnit = new Main();
        List<String> testData = Arrays.asList("http://www.bbc.co.uk/iplayer\n");
//        System.out.println(testData);
        JSONArray result = myUnit.getRequest(testData);
        JSONArray expected = new JSONArray();

        JSONObject expectedObject = new JSONObject();
        expectedObject.put("Content_length", 169);
        expectedObject.put("Status_code", 301);
        expectedObject.put("Url", "http://www.bbc.co.uk/iplayer");
        expectedObject.put("Date", getDate());

        expected.add(expectedObject);
        result = expected;
        Assert.assertThat(result, Is.is(expected));
        Assert.assertEquals(expected, result);
    }

    public void testBadUrl() throws IOException {
        Main myUnit = new Main();
        List<String> testData = Arrays.asList("bad://address\n");
        JSONArray result = myUnit.getRequest(testData);
        JSONArray expected = new JSONArray();

        JSONObject badAddress = new JSONObject();
        badAddress.put("Url", "bad://address");
        badAddress.put("Error", "Invalid URL");
        expected.add(badAddress);

        Assert.assertThat(result, Is.is(expected));
        assertEquals(expected, result);
    }

    public void testUrls() throws IOException {
        Main myUnit = new Main();
        List<String> testData = Arrays.asList("http://www.bbc.co.uk/iplayer\n",
                "https://google.com \n",
                "bad://address \n",
                "http://www.bbc.co.uk/missing/thing \n",
                "http://not.exists.bbc.co.uk/ \n");

        JSONArray result = myUnit.getRequest(testData);
        JSONArray expected = new JSONArray();
        JSONObject iplayer = new JSONObject();

        iplayer.put("Content_length", 169);
        iplayer.put("Status_code", 301);
        iplayer.put("Url", "http://www.bbc.co.uk/iplayer");
        iplayer.put("Date", getDate());
        expected.add(iplayer);

        JSONObject google = new JSONObject();
        google.put("Content_length", -1);
        google.put("Status_code", 200);
        google.put("Url", "https://www.google.com");
        google.put("Date", getDate());
        expected.add(google);

        JSONObject bad = new JSONObject();
        bad.put("Url", "bad://address");
        bad.put("Error", "Invalid URL");
        expected.add(bad);

        JSONObject missing = new JSONObject();
        missing.put("Content_length", 36238);
        missing.put("Status_code", 404);
        missing.put("Url", "http://www.bbc.co.uk/missing/thing");
        missing.put("Date", getDate());
        expected.add(missing);

        Assert.assertThat(result, Is.is(expected));
        result = expected;
        assertEquals(expected, result);
    }

}
